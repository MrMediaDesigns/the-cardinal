﻿/**
	Controls the buttons on the main menu
**/

#pragma strict
import UnityEngine.SceneManagement;
import UnityEngine.UI;

//Music button assets
var musicButton : GameObject;
var musicButtonOn : UI.Image;
var musicButtonOff : UI.Image;
var musicStatus : boolean = true;


function Start () {

}

function Update () {

}

function startGame()
{
	SceneManager.LoadScene("Level One");
}

function returnToMenu()
{
	SceneManager.LoadScene("Menu");
}

function openOptions()
{
	SceneManager.LoadScene("Options");
}

function musicToggle()
{
	if (musicStatus)
	{
		//toggle music
		//musicButton.image.sprite = musicButtonOn;
	}
	else
	{
		//musicButton.image.sprite = musicButtonOff;
	}
}